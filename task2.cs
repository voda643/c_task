using System;
using System.IO;

class Program
{
    static void Main()
    {
        string[] inputLines = File.ReadAllLines("input.txt");

        int n = int.Parse(inputLines[0]);

        int[,] sudoku = new int[n * n, n * n];

        for (int i = 1; i <= n * n; i++)
        {
            string[] row = inputLines[i].Split();
            for (int j = 0; j < n * n; j++)
            {
                sudoku[i - 1, j] = int.Parse(row[j]);
            }
        }

        bool isValid = IsSudokuCorrect(sudoku, n);

        File.WriteAllText("OUTPUT.TXT", isValid ? "Correct" : "Incorrect");
    }

    static bool IsSudokuCorrect(int[,] sudoku, int n)
    {
   
        for (int i = 0; i < n * n; i++)
        {
            if (!IsSetValid(GetRow(sudoku, i)) || !IsSetValid(GetColumn(sudoku, i)))
            {
                return false;
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (!IsSetValid(GetSquare(sudoku, i * n, j * n, n)))
                {
                    return false;
                }
            }
        }

        return true;
    }

    static int[] GetRow(int[,] sudoku, int row)
    {
        int size = sudoku.GetLength(1);
        int[] result = new int[size];
        for (int i = 0; i < size; i++)
        {
            result[i] = sudoku[row, i];
        }
        return result;
    }

    static int[] GetColumn(int[,] sudoku, int col)
    {
        int size = sudoku.GetLength(0);
        int[] result = new int[size];
        for (int i = 0; i < size; i++)
        {
            result[i] = sudoku[i, col];
        }
        return result;
    }

    static int[] GetSquare(int[,] sudoku, int startRow, int startCol, int n)
    {
        int size = n * n;
        int[] result = new int[size];
        int index = 0;
        for (int i = startRow; i < startRow + n; i++)
        {
            for (int j = startCol; j < startCol + n; j++)
            {
                result[index++] = sudoku[i, j];
            }
        }
        return result;
    }

    static bool IsSetValid(int[] set)
    {
        int size = set.Length;
        bool[] visited = new bool[size + 1];
        foreach (var num in set)
        {
            if (num < 1 || num > size || visited[num])
            {
                return false;
            }
            visited[num] = true;
        }
        return true;
    }
}
