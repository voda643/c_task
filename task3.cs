using System;
using System.IO;

class Program
{
    static void Main()
    {
        string[] inputLines = File.ReadAllLines("input.txt");

        int N = int.Parse(inputLines[0]);

        double[,] matrix = new double[N, N + 1];

        for (int i = 1; i <= N; i++)
        {
            string[] coefficients = inputLines[i].Split();
            for (int j = 0; j <= N; j++)
            {
                matrix[i - 1, j] = double.Parse(coefficients[j]);
            }
        }

        double[] solution = Gauss(matrix);

        for (int i = 0; i < N; i++)
        {
            solution[i] = Math.Ceiling(solution[i]);
        }

        File.WriteAllText("OUTPUT.TXT", string.Join(" ", solution));
    }

    static double[] Gauss(double[,] matrix)
    {
        int N = matrix.GetLength(0);

        for (int i = 0; i < N; i++)
        {
            double divisor = matrix[i, i];
            for (int j = 0; j <= N; j++)
            {
                matrix[i, j] /= divisor;
            }

            for (int k = 0; k < N; k++)
            {
                if (k != i)
                {
                    double multiplier = matrix[k, i];
                    for (int j = 0; j <= N; j++)
                    {
                        matrix[k, j] -= multiplier * matrix[i, j];
                    }
                }
            }
        }

        double[] solution = new double[N];
        for (int i = 0; i < N; i++)
        {
            solution[i] = matrix[i, N];
        }

        return solution;
    }
}
