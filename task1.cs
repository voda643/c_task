using System;
using System.IO;

class Program
{
    static void Main()
    {
        string[] inputLines = File.ReadAllLines("input.txt");
        string[] wh = inputLines[0].Split();
        int w = int.Parse(wh[0]);
        int h = int.Parse(wh[1]);
        int n = int.Parse(inputLines[1]);

        bool[,] canvas = new bool[w, h];

        for (int i = 2; i < n + 2; i++)
        {
            string[] rectangleCoords = inputLines[i].Split();
            int x1 = int.Parse(rectangleCoords[0]);
            int y1 = int.Parse(rectangleCoords[1]);
            int x2 = int.Parse(rectangleCoords[2]);
            int y2 = int.Parse(rectangleCoords[3]);

            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    canvas[x, y] = true;
                }
            }
        }

        int whiteArea = 0;
        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                if (!canvas[x, y])
                {
                    whiteArea++;
                }
            }
        }

        File.WriteAllText("output.txt", whiteArea.ToString());
    }
}
